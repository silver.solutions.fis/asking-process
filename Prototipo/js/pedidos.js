$(document).ready(function(){
    //Json para los tipos de platos
    var platosFondo = {"0" : "Ravioli Jamón Alcachofa","1" : "Lasagna Napolitana", "2" : "Chorrillana Mediterranea", "3" : "Pastel de Choclo"};
    var entradas = {"0" : "Cebiche de Champiñones","1" : "Tequeños", "2" : "Paltas rellenas", "3" : "Tomate relleno"};
    var sopas = {"0" : "Crema de espárragos","1" : "Crema de Brócoli", "2" : "Sopa de Verduras", "3" : "Casuela de Vacuno"};
    var sandwich = {"0" : "Barros Luco","1" : "Churrasco Italiano", "2" : "Hamburguesa de Pollo", "3" : "Sandwish de Verduras"};
    var tablas = {"0" : "Surtido frio","1" : "Tabla de carne", "2" : "Papas Bravas", "3" : "Mini Arrollados"};

    //evento click para los platos
    $("#chkPlato").click(function(){
        $("#formPlatos").toggleClass("d-none");
    });
    //evento click para los bebestibles
    $("#chkBebestible").click(function(){
        /* if ($('#chkBebestibles').is(':checked')) {
            console.log('Si');
        } else {
            console.log('No');
        } */
        $("#formBebestibles").toggleClass("d-none");
    });
    //evento click para las salsas
    $("#chkSalsa").click(function(){
        $("#formSalsas").toggleClass("d-none");
    });
    //evento click para otros
    $("#chkOtro").click(function(){
        //$("#formOtros").toggleClass("d-none");
    });

    //evento click para boton agregar
    $("#btnAgregar").click(function(){
        //Se hace visible
        $("#btnQuitar").removeClass("d-none");
        //Concatena el html (div>labels>selects>options)
        $("#cardPlatos").append($('<hr/>')).append(
            $('<div/>',{
                class: 'row mt-3',
                html: $('<div/>',{
                    class: 'col-sm-5',
                    html: $('<label/>',{
                        class: 'text-light',
                        for: "selectCategoria",
                        html: "Seleccione Categoría:" 
                    })
                })
            }).append(
                $('<div/>',{
                    class: 'col-sm-7',
                    html: $('<select/>',{
                        class: 'form-control',
                        html: $('<option/>',{
                            value: '0',
                            html: "Entrada"
                        })
                    }).append(
                        $('<option/>',{
                            value: '1',
                            html: "Plato de Fondo"
                        })
                    ).append(
                        $('<option/>',{
                            value: '2',
                            html: "Sopas"
                        })
                    ).append(
                        $('<option/>',{
                            value: '3',
                            html: "Sandwish"
                        })
                    ).append(
                        $('<option/>',{
                            value: '1',
                            html: "Tablas"
                        })
                    )
                })
            )
        ).append(
            $('<div/>',{
                class: 'row mt-3',
                html: $('<div/>',{
                    class: 'col-sm-4',
                    html: $('<label/>',{
                        class: 'text-light',
                        for: "selectPlato",
                        html: "Seleccione Plato/Cant:" 
                    })
                })
            }).append(
                $('<div/>',{
                    class: 'col-sm-6',
                    html: $('<select/>',{
                        class: 'form-control',
                        html: $('<option/>',{
                            value: '0',
                            html: "Cebiche de Champiñones"
                        })
                    }).append(
                        $('<option/>',{
                            value: '1',
                            html: "Tequeños"
                        })
                    ).append(
                        $('<option/>',{
                            value: '2',
                            html: "Paltas rellenas"
                        })
                    ).append(
                        $('<option/>',{
                            value: '3',
                            html: "Tomate relleno"
                        })
                    )
                })
            ).append(
                $('<div/>',{
                    class: 'col-sm-2',
                    html: $('<input/>',{
                        class: 'form-control',
                        name: 'cantPlatos',
                        type: 'text',
                        title: 'Cantidad de Platos'
                    })
                })
            )
        );
    });

    //evento click para cambiar el listado de los platos segun el tipo seleccionado
    $("#selectCategoriaPlato").change(function(){
        //obtiene el value
        var selectedCategoria = $(this).children("option:selected").val();
        //elimina los datos anteriores
        $('#selectPlato').find('option').remove();
        //carga los datos según el tipo de dato seleccionado
        switch (selectedCategoria) { 
            case '0': 
                $.each(entradas,function(key, value){
                    $('#selectPlato').append('<option value=' + key + '>' + value + '</option>');
                });
                break;
            case '1': 
                $.each(platosFondo,function(key, value){
                    $('#selectPlato').append('<option value=' + key + '>' + value + '</option>');
                });
                break;
            case '2':
                $.each(sopas,function(key, value){
                    $('#selectPlato').append('<option value=' + key + '>' + value + '</option>');
                });
                break;
            case '3': 
                $.each(sandwich,function(key, value){
                    $('#selectPlato').append('<option value=' + key + '>' + value + '</option>');
                });
                break;
            case '4':
                $.each(tablas,function(key, value){
                    $('#selectPlato').append('<option value=' + key + '>' + value + '</option>');
                });
                break;
            default:
                alert('Error en cargar Platos.');
        }
    });
});