$(document).ready(function(){
    $(".calificacion").starRating({
        initialRating: 3,
        starSize: 100,
        minRating: 1,
        useGradient: false,
        activeColor: "orange",
        ratedColor: "gold",
        useFullStars: true,
        disableAfterRate: false,
        starShape: "rounded"
    });

    $(".calificacion").click(function(){
        /* var calificacion = $('.calificacion').starRating('getRating'); */
        $('#clasificacionModal').modal('show')
    });
});